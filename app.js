const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const net = require('net')
const colors = require('colors/safe');

//Variabelen Log Filter
const logs = {};
let lastLogMessage = '';

const server = express();
server.use(bodyParser.json());

server.listen(8080, () => {
    console.log(colors.green('   ____  _____ _______ _____ __  __  ____  _____      _____  _____   _____ '))
    console.log(colors.green('  / __ \\|  __ \\__   __|_   _|  \\/  |/ __ \\|  __ \\    |  __ \\|  __ \\ / ____|'))
    console.log(colors.green(' | |  | | |__) | | |    | | | \\  / | |  | | |  | |   | |__) | |  | | (___  '))
    console.log(colors.green(' | |  | |  ___/  | |    | | | |\\/| | |  | | |  | |   |  _  /| |  | |\\___ \\ '))
    console.log(colors.green(' | |__| | |      | |   _| |_| |  | | |__| | |__| |   | | \\ \\| |__| |____) |'))
    console.log(colors.green('  \\____/|_|      |_|  |_____|_|  |_|\\____/|_____/    |_|  \\_\\_____/|_____/ '))
    console.log(colors.yellow(''))
    console.log(colors.yellow('              __  __   _    ____  ___ _____ _   _ ___ ___ ___  '))
    console.log(colors.yellow('             |  \\/  | /_\\  |_  / / __|_   _| | | |   \\_ _/ _ \\ '))
    console.log(colors.yellow('             | |\\/| |/ _ \\  / / _\\__ \\ | | | |_| | |) | | (_) |'))
    console.log(colors.yellow('             |_|  |_/_/ \\_\\/___(_)___/ |_|  \\___/|___/___\\___/ '))
    console.log(colors.yellow(''))
    console.log(colors.yellow(''))
    console.log(colors.green('Server gestart op http://localhost:8080'))
    console.log(colors.yellow(' '))
});

fs.readFile('config.json', 'utf8', (err, data) => {
    if (err) {
        console.log(colors.red('Fout bij het lezen van het configuratiebestand:', err));
        return;
    }

    const config = JSON.parse(data);
    const { host, port } = config;

    tcpClient = net.connect({ host, port }, () => {

        console.log(colors.grey('Verbinden met Optimod via', host, 'en poort', port));
        console.log(colors.green('Verbinding succesvol.'));
    });
    tcpClient.on('error', (err) => {
        console.log(colors.grey('Verbinden met Optimod via', host, 'en poort', port));
        console.log(colors.red('Kan niet verbinden met de Optimod, controleer of het ip-adres en/of poort nummer overeen komen.'));
    });
});

function formatDuration(milliseconds) {
    const minutes = Math.floor(milliseconds / 60000);
    const seconds = ((milliseconds % 60000) / 1000).toFixed(0);
    return `${minutes}:${(seconds < 10 ? '0' : '')}${seconds}`;
}

server.post('/post-json', (req, res) => {
    const durationTime = req.body.params.current.mix['duration.msec.i'];
    function filteredConsoleLog(logMessage) {
        if (logMessage !== lastLogMessage) {
            logs[logMessage] = Date.now();
            lastLogMessage = logMessage;
            console.log(logMessage);
        }
    }
    if (durationTime > 90000) {
        const trackLog = colors.blue('[TRACK] NOW PLAYING:') + ` ${req.body.params.current['artistName.s']} - ${req.body.params.current['titleName.s']}` + ' [DURATION: ' + formatDuration(req.body.params.current.mix['duration.msec.i']) + ']';
        filteredConsoleLog(trackLog);
        const artistName = req.body.params.current['artistName.s'];
        const titleName = req.body.params.current['titleName.s'];
        const nowPlaying = `DSP=${artistName} - ${titleName}`;
        tcpClient.write(nowPlaying);
        tcpClient.on('data', (data) => {
            console.log(data.toString());
        });
        // console.log(nowPlaying)

        fs.readFile('config.json', 'utf8', (err, data) => {
            if (err) {
                console.log(colors.red('Fout bij het lezen van het configuratiebestand:', err));
                return;
            }
        });
    } else {
        const jingleLog = colors.grey('[JINGLE] NOW PLAYING:') + ` ${req.body.params.current['artistName.s']} - ${req.body.params.current['titleName.s']}` + ' [DURATION: ' + formatDuration(req.body.params.current.mix['duration.msec.i']) + ']';
        filteredConsoleLog(jingleLog);

    }
    res.json({ success: true });
});
