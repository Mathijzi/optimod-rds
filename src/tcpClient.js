// tcpClient.js
const net = require('net');
const fs = require('fs');
const colors = require('colors/safe');

const { formatDuration } = require('../src/utils');

let tcpClient;

function initializeTCPClient() {
    fs.readFile('./src/config.json', 'utf8', (err, data) => {
        if (err) {
            console.log(colors.red('Fout bij het lezen van het configuratiebestand:', err));
            return;
        }

        const config = JSON.parse(data);
        const { host, port } = config;

        tcpClient = net.connect({ host, port }, () => {
            console.log(colors.grey('Verbinden met Optimod via', host, 'en poort', port));
            console.log(colors.green('Verbinding succesvol.'));
        });

        tcpClient.on('error', (err) => {
            console.log(colors.grey('Verbinden met Optimod via', host, 'en poort', port));
            console.log(colors.red('Kan niet verbinden met de Optimod, controleer of het ip-adres en/of poort nummer overeen komen.'));
        });
    });
}

module.exports = { tcpClient, initializeTCPClient };
