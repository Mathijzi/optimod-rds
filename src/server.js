// server.js
const fs = require('fs');
const colors = require('colors/safe');

const { formatDuration } = require('../src/utils');
const { tcpClient } = require('../src/tcpClient');

const logs = {}; // Object om tijdstempels van logs bij te houden
let lastLogMessage = ''; // Variabele om het laatst gelogde bericht bij te houden

function filteredConsoleLog(logMessage) {
    if (logMessage !== lastLogMessage) {
        logs[logMessage] = Date.now();
        lastLogMessage = logMessage;
        console.log(logMessage);
    }
}

function handleJsonPost(req, res) {
    const jsonData = req.body;
    const tcpCommand = req.body.tcpCommand;
    const durationTime = req.body.params.current.mix['duration.msec.i'];

    if (durationTime > 90000) {
        const trackLog = colors.blue('[TRACK] NOW PLAYING:') + ` ${req.body.params.current['artistName.s']} - ${req.body.params.current['titleName.s']}` + ' [DURATION: ' + formatDuration(req.body.params.current.mix['duration.msec.i']) + ']';
        filteredConsoleLog(trackLog);
        const artistName = req.body.params.current['artistName.s'];
        const titleName = req.body.params.current['titleName.s'];
        const nowPlaying = `DSP=${artistName} - ${titleName}`;
        tcpClient.write(nowPlaying);
        tcpClient.on('data', (data) => {
            console.log(data.toString());
        });
        // console.log(nowPlaying)

        fs.readFile('config.json', 'utf8', (err, data) => {
            if (err) {
                console.log(colors.red('Fout bij het lezen van het configuratiebestand:', err));
                return;
            }
        });
    } else {
        const jingleLog = colors.grey('[JINGLE] NOW PLAYING:') + ` ${req.body.params.current['artistName.s']} - ${req.body.params.current['titleName.s']}` + ' [DURATION: ' + formatDuration(req.body.params.current.mix['duration.msec.i']) + ']';
        filteredConsoleLog(jingleLog);

    }
    res.json({ success: true });
}

function startServer(server) {
    const port = 8080;
    server.listen(port, () => {
        console.log(colors.green('Server gestart op http://localhost:' + port));
        console.log(colors.yellow(''));
    });
}

module.exports = { handleJsonPost, startServer };
