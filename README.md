![MAZ STUDIO LOGO](https://iili.io/HZTrwwF.png)

Post JSON, filter music & station imaging & Send data to Orban Optimod terminal RDS.

## Installation

This is a [Node.js](https://nodejs.org/en/) module available through the
[npm registry](https://www.npmjs.com/).

Before installing, [download and install Node.js](https://nodejs.org/en/download/).

## Quick Start

  Install dependencies:

```console
$ npm install
```

  Start the server:

```console
$ npm start
```

